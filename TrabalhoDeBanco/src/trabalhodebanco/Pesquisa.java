/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author MaisUmaVez
 */
public class Pesquisa {
    public static ArrayList<Aluno> pesquisaAlunoPorNome(String nome) {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE NOME_ALUNO = ? AND  ID_USUARIO = ?";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    public static ArrayList<Atividade> pesquisaAtividadePorNome(String nome) {
        String selectSQL = "SELECT * FROM ATIVIDADEBD3 WHERE NOME_ATIVIDADE = ? AND  ID_USUARIO = ?";
        ArrayList<Atividade> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Atividade f = new Atividade();
                f.setId(rs.getInt("ID_ATIVIDADE"));
                f.setNome(rs.getString("NOME_ATIVIDADE"));
                f.setData(rs.getString("DATA"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    public static ArrayList<Professor> pesquisaProfessorPorNome(String nome) {
        String selectSQL = "SELECT * FROM PROFESSORBD3 WHERE NOME_PROFESSOR = ? AND  ID_USUARIO = ?";
        ArrayList<Professor> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Professor f = new Professor();
                f.setNome(rs.getString("NOME_PROFESSOR"));
                f.setArea(rs.getString("AREA"));
                f.setId(rs.getInt("ID_PROFESSOR"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Turma> pesquisaTurmaPorNome(String nome) {
        String selectSQL = "SELECT * FROM TURMASBD3 WHERE NOME_TURMA = ? AND  ID_USUARIO = ?";
        ArrayList<Turma> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Turma f = new Turma();
                f.setId(rs.getInt("ID_TURMA"));
                f.setNome(rs.getString("NOME_TURMA"));
                f.setSala(rs.getString("SALA"));
                f.setNivel(rs.getString("NIVEL"));
                f.setIdProfessor(rs.getInt("ID_PROFESSOR"));
                f.setNomeProfessor(Professor.getNomePeloId(f.getIdProfessor()));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Turma> pesquisaTurmaPorProfessor(String nome) {
        String selectSQL = "SELECT * FROM TURMASBD3 INNER JOIN PROFESSORBD3 ON TURMASBD3.ID_PROFESSOR = PROFESSORBD3.ID_PROFESSOR AND PROFESSORBD3.NOME_PROFESSOR = ? WHERE TURMASBD3.ID_USUARIO = ?";
        ArrayList<Turma> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Turma f = new Turma();
                f.setId(rs.getInt("ID_TURMA"));
                f.setNome(rs.getString("NOME_TURMA"));
                f.setSala(rs.getString("SALA"));
                f.setNivel(rs.getString("NIVEL"));
                f.setIdProfessor(rs.getInt("ID_PROFESSOR"));
                f.setNomeProfessor(Professor.getNomePeloId(f.getIdProfessor()));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Atividade> pesquisaAtividadePorAluno(String nome) {
        String selectSQL = "SELECT * FROM ATIVIDADEBD3 INNER JOIN ALUNO_ATIVIDADEBD3 ON ALUNO_ATIVIDADEBD3.ID_ATIVIDADE = ATIVIDADEBD3.ID_ATIVIDADE INNER JOIN ALUNOBD3 ON ALUNOBD3.NOME_ALUNO = ? AND ALUNOBD3.ID_ALUNO = ALUNO_ATIVIDADEBD3.ID_ALUNO WHERE ALUNOBD3.ID_USUARIO = ?";
        ArrayList<Atividade> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Atividade f = new Atividade();
                f.setId(rs.getInt("ID_ATIVIDADE"));
                f.setNome(rs.getString("NOME_ATIVIDADE"));
                f.setData(rs.getString("DATA"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Turma> pesquisaTurmaPorArea(String nome) {
        String selectSQL = "SELECT * FROM TURMASBD3 INNER JOIN PROFESSORBD3 ON TURMASBD3.ID_PROFESSOR = PROFESSORBD3.ID_PROFESSOR AND PROFESSORBD3.AREA = ? WHERE TURMASBD3.ID_USUARIO = ?";
        ArrayList<Turma> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Turma f = new Turma();
                f.setId(rs.getInt("ID_TURMA"));
                f.setNome(rs.getString("NOME_TURMA"));
                f.setSala(rs.getString("SALA"));
                f.setNivel(rs.getString("NIVEL"));
                f.setIdProfessor(rs.getInt("ID_PROFESSOR"));
                f.setNomeProfessor(Professor.getNomePeloId(f.getIdProfessor()));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Atividade> pesquisaAtividadePorData(String nome) {
        String selectSQL = "SELECT * FROM ATIVIDADEBD3 WHERE DATA = ? AND  ID_USUARIO = ?";
        ArrayList<Atividade> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setString(1, nome);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Atividade f = new Atividade();
                f.setId(rs.getInt("ID_ATIVIDADE"));
                f.setNome(rs.getString("NOME_ATIVIDADE"));
                f.setData(rs.getString("DATA"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Aluno> pesquisaAlunoPorMatricula(int matricula) {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE MATRICULA = ? AND  ID_USUARIO = ?";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, matricula);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Turma> pesquisaTurmas() {
        String selectSQL = "SELECT * FROM TURMASBD3 WHERE  ID_USUARIO = ?";
        ArrayList<Turma> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Turma f = new Turma();
                f.setId(rs.getInt("ID_TURMA"));
                f.setNome(rs.getString("NOME_TURMA"));
                f.setSala(rs.getString("SALA"));
                f.setNivel(rs.getString("NIVEL"));
                f.setIdProfessor(rs.getInt("ID_PROFESSOR"));
                f.setNomeProfessor(Professor.getNomePeloId(f.getIdProfessor()));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Aluno> pesquisaAlunoSem() {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ALUNOBD3.ID_ALUNO NOT IN (SELECT ID_ALUNO FROM ALUNO_ATIVIDADEBD3) AND  ID_USUARIO = ?";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Turma> pesquisaTurmasSem() {
        String selectSQL = "SELECT * FROM TURMASBD3 WHERE TURMASBD3.ID_TURMA NOT IN (SELECT ID_TURMA FROM TURMA_ALUNOBD3) AND  ID_USUARIO = ?";
        ArrayList<Turma> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Turma f = new Turma();
                f.setId(rs.getInt("ID_TURMA"));
                f.setNome(rs.getString("NOME_TURMA"));
                f.setSala(rs.getString("SALA"));
                f.setNivel(rs.getString("NIVEL"));
                f.setIdProfessor(rs.getInt("ID_PROFESSOR"));
                f.setNomeProfessor(Professor.getNomePeloId(f.getIdProfessor()));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Professor> pesquisaProfessorSem() {
        String selectSQL = "SELECT * FROM PROFESSORBD3 WHERE PROFESSORBD3.ID_PROFESSOR NOT IN (SELECT ID_PROFESSOR FROM TURMASBD3) AND  ID_USUARIO = ?";
        ArrayList<Professor> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Professor f = new Professor();
                f.setNome(rs.getString("NOME_PROFESSOR"));
                f.setArea(rs.getString("AREA"));
                f.setId(rs.getInt("ID_PROFESSOR"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
}
