/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author MaisUmaVez
 */
public class Aluno {
    private String nome;
    private int matricula,id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO ALUNOBD3 (ID_ALUNO,NOME_ALUNO,MATRICULA ,ID_USUARIO) VALUES (SEQ_ALUNOBD3_ID_ALUNO.nextval,?,?,?)";
        

        try {
            
            String generatedColumns[] = {"ID_ALUNO"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL,generatedColumns);
            preparedStatement.setString(1, this.nome);
            preparedStatement.setInt(2, this.matricula);
            preparedStatement.setInt(3, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setId(chaveGerada);
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<Aluno> getAll() {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ID_USUARIO = ?";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    
    
//    public boolean update(String nome) {
//      	Conexao c = new Conexao();
//        Connection dbConnection = c.getConexao();
//        PreparedStatement ps = null;
//
//        String insertTableSQL = "UPDATE MARCAJDBC3 SET NOME =? WHERE NOME = ?";
//        
//
//        try {
//            ps = dbConnection.prepareStatement(insertTableSQL);
//            ps.setString(1, nome);
//            ps.setString(2, this.nome);
//          
//            
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            
//            e.printStackTrace();
//            return false;
//        }finally{
//            c.desconecta(); 
//        }        
//        return true;
//    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM ALUNOBD3 WHERE MATRICULA = ? AND ID_USUARIO = ?";        
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.matricula);
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            deleteDaTurma();
            deleteDaAtividade();
            //deletePendrives();
            ps.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }      
        
        return true;
        
    }
    
    private void deleteDaTurma() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM TURMA_ALUNOBD3 WHERE ID_ALUNO = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }        
        
    }
    
    private void deleteDaAtividade() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM ALUNO_ATIVIDADEBD3 WHERE ID_ALUNO = ? ";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        
    }
//    public boolean deletePendrives() {
//        Conexao c = new Conexao();
//        Connection dbConnection = c.getConexao();
//        PreparedStatement ps = null;
//
//        String insertTableSQL = "DELETE FROM PRODUTOJDBC3 WHERE ID_MARCA  = ? ";        
//
//        try {
//            ps = dbConnection.prepareStatement(insertTableSQL);
//            ps.setInt(1, this.getId());
//          
//            
//            ps.executeUpdate();
//        } catch (SQLException e) {
//           
//            e.printStackTrace();
//            return false;
//        }finally{
//            c.desconecta(); 
//        }        
//        return true;
//        
//    }
    
    public static boolean verificaExisteBanco(int id) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE MATRICULA = ? AND ID_USUARIO = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id); 
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery(); 
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public static boolean verificaExisteBanco2() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        

        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ID_USUARIO = ?";
        PreparedStatement st;
        try { 
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    
}
