/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class TelaLoginController implements Initializable {

    @FXML
    private PasswordField senha;
    @FXML
    private TextField nome;
    @FXML
    private Label mensagemResposta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void logar(ActionEvent event) {
        if(Usuario.verificaExisteBanco2(nome.getText(), senha.getText())){
            TrabalhoDeBanco.dadosGerais.setUserLogado(Usuario.getId(nome.getText()));
            Parent root;
            try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
            } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
            }
        }else{
            mensagemResposta.setTextFill(Color.WHITE);
            mensagemResposta.setText("Nome Ou Senha Incorretos");
        }
    }

    @FXML
    private void cadastrar(ActionEvent event) {
            Parent root;
            try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("NovoUsuario.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
            } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
            }
    }
    
}
