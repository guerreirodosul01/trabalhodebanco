/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class GerenciaProfesssorController implements Initializable {

    @FXML
    private Label mensagemResposta;
    @FXML
    private TableView<Professor> tabela;
    @FXML
    private TableColumn<Professor, String> nomeCol;
    @FXML
    private TableColumn<Professor, String> areaCol;
    @FXML
    private Button adicionaProfessorButton;
    @FXML
    private TextField area;
    @FXML
    private TextField nome;
    @FXML
    private Button excluirButton;
    
    private ObservableList<Professor> professorList;
    
    private Professor tipo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          atualiza();
          excluirButton.setDisable(true);
    }    

    @FXML
    private void voltaPaginaInicial(ActionEvent event) {
        Parent root;
        try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }

    @FXML
    private void limpaInputs(ActionEvent event) {
        tabela.getSelectionModel().clearSelection();
        nome.setText("");
        area.setText("");
        
        excluirButton.setDisable(true);
        mensagemResposta.setText("");
    }

    @FXML
    private void selecionado(MouseEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
            excluirButton.setDisable(false);
            
            Professor tipoSelecionado = tabela.getSelectionModel().getSelectedItem();
            
        }
    }

    @FXML
    private void adicionaProfessor(ActionEvent event) {
        if(!nome.getText().isEmpty() && !area.getText().isEmpty()){
            if(true){
                   tipo = new Professor();
                   tipo.setNome(nome.getText());
                   tipo.setArea(area.getText());
                   boolean resposta = tipo.insert();

                   
                   
                   if(resposta){
                        mensagemResposta.setTextFill(Color.GREEN);
                        mensagemResposta.setText("Dado Foi Cadastrado Corretamente");
                    }else{
                        mensagemResposta.setTextFill(Color.RED);
                        mensagemResposta.setText("Erro Ao Cadastrar");
                    }
                   
                   nome.setText("");
                   area.setText("");
                   
                   
                   professorList.add(tipo);
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Nunca vai entrar aqui");
            }
        }else{
           mensagemResposta.setTextFill(Color.RED);
           mensagemResposta.setText("Não Deixe Nada Em Braco"); 
            
        }
    }

    @FXML
    private void excluir(ActionEvent event) {
        Professor tipoSelecionado = tabela.getSelectionModel().getSelectedItem();
        boolean resposta = tipoSelecionado.delete();
        professorList.remove(tipoSelecionado);
        limpaInputs(event);
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Professor Excluido Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }
    }
    
    public void atualiza(){
        
        professorList = tabela.getItems();
        professorList.clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        areaCol.setCellValueFactory(new PropertyValueFactory<>("area"));
        Professor.getAll().forEach((a) -> {
            professorList.add(a);
        });
        
    }
    
}
