/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class GerenciaTurmasController implements Initializable {

    @FXML
    private TableView<Turma> turmaTable;
    @FXML
    private TableColumn<Turma, String> nomeTurmaCol;
    @FXML
    private TableColumn<Turma, String> salaTurmaCol;
    @FXML
    private TableColumn<Turma, String> nivelTurmaCol;
    @FXML
    private TableColumn<Turma, String> professorTurmaCol;
    @FXML
    private TableView<Aluno> naoAlunosTable;
    @FXML
    private TableColumn<Aluno, String> nomeNaoAlunosCol;
    @FXML
    private TableColumn<Aluno, Integer> matriculaNaoAlunosCol;
    @FXML
    private TableView<Aluno> alunosTable;
    @FXML
    private TableColumn<Aluno, String> nomeAlunosCol;
    @FXML
    private TableColumn<Aluno, Integer> matriculaAlunosCol;
    @FXML
    private TextField nome;
    @FXML
    private Button cadastrarButton;
    @FXML
    private Label mensagemResposta;
    @FXML
    private Button excluirButton;
    @FXML
    private Button adiconarAoGrupoButton;
    @FXML
    private Button removerButton;
    @FXML
    private TextField sala;
    @FXML
    private TextField nivel;
    @FXML
    private ComboBox<String> professor;

    private ObservableList<Turma> grupoList;
    private ObservableList<Aluno> membrosList;
    private ObservableList<Aluno> naoMembrosList;
    private ObservableList<String> tiposList;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualiza();
        excluirButton.setDisable(true);
        adiconarAoGrupoButton.setDisable(true);
        removerButton.setDisable(true);
        tiposList = professor.getItems();
        Professor.getAll().forEach((a) -> {
            tiposList.add(a.getNome());
        });
    }    

    public void atualiza(){
        
        grupoList = turmaTable.getItems();
        grupoList.clear();
        nomeTurmaCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        salaTurmaCol.setCellValueFactory(new PropertyValueFactory<>("sala"));
        nivelTurmaCol.setCellValueFactory(new PropertyValueFactory<>("nivel"));
        professorTurmaCol.setCellValueFactory(new PropertyValueFactory<>("nomeProfessor"));
        Turma.getAll().forEach((a) -> {
            grupoList.add(a);
        });
        
        
        
        
    }
    
    public void atualizaMembros(){
        membrosList = alunosTable.getItems();
        membrosList.clear();
        nomeAlunosCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        matriculaAlunosCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));     
        Turma.getAllMembros(turmaTable.getSelectionModel().getSelectedItem().getId()).forEach((a) -> {
            membrosList.add(a);
        });
        
        naoMembrosList = naoAlunosTable.getItems();
        naoMembrosList.clear();
        nomeNaoAlunosCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        matriculaNaoAlunosCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));    
        Turma.getAllNaoMembros(turmaTable.getSelectionModel().getSelectedItem().getId()).forEach((a) -> {
            naoMembrosList.add(a);
        });

        
    }

    @FXML
    private void cadastrar(ActionEvent event) {
        if(!nome.getText().isEmpty() && !sala.getText().isEmpty() && !nivel.getText().isEmpty() && professor.getSelectionModel().getSelectedItem() != null){
            if(Turma.verificaExisteBanco(nome.getText())){
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Turma Já Em Uso");
            }else{
            Turma turma = new Turma();
            turma.setNome(nome.getText());
            turma.setSala(sala.getText());
            turma.setNivel(nivel.getText());
            turma.setNomeProfessor(professor.getSelectionModel().getSelectedItem());
            turma.setIdProfessor(Professor.getIdPeloNome(professor.getSelectionModel().getSelectedItem()));
            
            
            boolean resulta = turma.insert();
            if(resulta){
                mensagemResposta.setTextFill(Color.GREEN);
                mensagemResposta.setText("Cadastrado Com Sucesso");
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Erro Ao Cadastrar");
            }
            }
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Preencha Coretamente");
        }
        atualiza();
    }

    @FXML
    private void excluirTurma(ActionEvent event) {
        alunosTable.getSelectionModel().clearSelection();
        naoAlunosTable.getSelectionModel().clearSelection();
        membrosList.clear();
        naoMembrosList.clear();
        excluirButton.setDisable(true);
        adiconarAoGrupoButton.setDisable(true);
        removerButton.setDisable(true);
        boolean resposta = turmaTable.getSelectionModel().getSelectedItem().delete();
        grupoList.remove(turmaTable.getSelectionModel().getSelectedItem());
        turmaTable.getSelectionModel().clearSelection();
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Grupo E Membros Foram Excluidos Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }
        
        atualiza();
    }

    @FXML
    private void adiconarAoGrupo(ActionEvent event) {
        boolean resposta = turmaTable.getSelectionModel().getSelectedItem().insertMembro(naoAlunosTable.getSelectionModel().getSelectedItem().getId());
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Adicionado Corretamente");
            alunosTable.getSelectionModel().clearSelection();
            naoAlunosTable.getSelectionModel().clearSelection();
            adiconarAoGrupoButton.setDisable(true);
            removerButton.setDisable(true);
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Adicionar");
        }
        atualizaMembros();
    }

    @FXML
    private void removerAluno(ActionEvent event) {
        boolean resposta = turmaTable.getSelectionModel().getSelectedItem().deleteMembro(alunosTable.getSelectionModel().getSelectedItem().getId());
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Removido Corretamente");
            alunosTable.getSelectionModel().clearSelection();
            naoAlunosTable.getSelectionModel().clearSelection();
            adiconarAoGrupoButton.setDisable(true);
            removerButton.setDisable(true);
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Remover");
        }
        atualizaMembros();
    }

    @FXML
    private void voltarInicio(ActionEvent event) {
        Parent root;
        try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }

    @FXML
    private void turmaSelecionado(MouseEvent event) {
        if(turmaTable.getSelectionModel().getSelectedItem() != null){
        atualizaMembros();
        excluirButton.setDisable(false);
        }
    }

    @FXML
    private void naoAlunoSelecionado(MouseEvent event) {
        if(naoAlunosTable.getSelectionModel().getSelectedItem() != null){
        adiconarAoGrupoButton.setDisable(false);
        }
    }

    @FXML
    private void alunoSelecionado(MouseEvent event) {
        if(alunosTable.getSelectionModel().getSelectedItem() != null){
        removerButton.setDisable(false);
        }
    }
    
}
