/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author MaisUmaVez
 */
public class Turma {
    private String sala, nome , nivel,nomeProfessor;
    private int id,idProfessor;

    public String getNomeProfessor() {
        return nomeProfessor;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }
    

    public int getIdProfessor() {
        return idProfessor;
    }

    public void setIdProfessor(int idProfessor) {
        this.idProfessor = idProfessor;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO TURMASBD3 (ID_TURMA,NOME_TURMA,SALA ,NIVEL,ID_USUARIO,ID_PROFESSOR) VALUES (SEQ_TURMAS_ID_TURMA.nextval,?,?,?,?,?)";
        

        try {
            
            String generatedColumns[] = {"ID_TURMA"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL,generatedColumns);
            preparedStatement.setString(1, this.nome);
            preparedStatement.setString(2, this.sala);
            preparedStatement.setString(3, this.nivel);
            preparedStatement.setInt(4, TrabalhoDeBanco.dadosGerais.getUserLogado());
            preparedStatement.setInt(5, this.idProfessor);
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setId(chaveGerada);
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<Turma> getAll() {
        String selectSQL = "SELECT * FROM TURMASBD3 WHERE ID_USUARIO = ?";
        ArrayList<Turma> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Turma f = new Turma();
                f.setId(rs.getInt("ID_TURMA"));
                f.setNome(rs.getString("NOME_TURMA"));
                f.setSala(rs.getString("SALA"));
                f.setNivel(rs.getString("NIVEL"));
                f.setIdProfessor(rs.getInt("ID_PROFESSOR"));
                f.setNomeProfessor(Professor.getNomePeloId(f.getIdProfessor()));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Aluno> getAllMembros(int grupo) {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ALUNOBD3.ID_ALUNO  IN (SELECT ID_ALUNO FROM TURMA_ALUNOBD3 WHERE  ID_TURMA = ?)  AND ID_USUARIO = ? ";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, grupo);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    public static ArrayList<Aluno> getAllNaoMembros(int grupo) {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ALUNOBD3.ID_ALUNO NOT IN (SELECT ID_ALUNO FROM TURMA_ALUNOBD3 WHERE  ID_TURMA = ?) AND  ID_USUARIO = ? ";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, grupo);
            st.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    
    
    
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM TURMASBD3 WHERE NOME_TURMA = ? AND ID_USUARIO = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nome);
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
          
            deleteMembros();
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public static boolean verificaExisteBanco(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM TURMASBD3 WHERE NOME_TURMA = ? AND ID_USUARIO  = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);// 
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public boolean insertMembro(int membro) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO TURMA_ALUNOBD3 (ID_ALUNO,ID_TURMA ) VALUES (?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, membro);
            
            preparedStatement.setInt(2, this.id);
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public boolean deleteMembro(int membro) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM TURMA_ALUNOBD3 WHERE ID_ALUNO = ? AND ID_TURMA = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, membro);
            ps.setInt(2, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public boolean deleteMembros() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM TURMA_ALUNOBD3 WHERE ID_TURMA = ? ";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
}
