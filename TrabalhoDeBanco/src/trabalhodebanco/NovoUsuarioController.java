/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class NovoUsuarioController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private PasswordField senha;
    @FXML
    private PasswordField reSenha;
    @FXML
    private Label mensagemResposta;
    @FXML
    private TextField escola;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrar(ActionEvent event) {
        if(!escola.getText().isEmpty()  && !nome.getText().isEmpty()  && !senha.getText().isEmpty() && !reSenha.getText().isEmpty()  && senha.getText().equals(reSenha.getText())){
            if(Usuario.verificaExisteBanco(nome.getText())){
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Usuario Já Em Uso");
            }else{
            Usuario user = new Usuario();
            user.setNome(nome.getText());
            user.setSenha(senha.getText());
            user.setEscola(escola.getText());
            boolean resulta = user.insert();
            if(resulta){
                mensagemResposta.setTextFill(Color.GREEN);
                mensagemResposta.setText("Cadastrado Com Sucesso");
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Erro Ao Cadastrar");
            }
            }
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Preencha Coretamente");
        }
    }

    @FXML
    private void logar(ActionEvent event) {
        Parent root;
        try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaLogin.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }
    
}
