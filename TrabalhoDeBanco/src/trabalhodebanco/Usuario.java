/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author MaisUmaVez
 */
class Usuario {
    private String nome,senha,escola;

    public String getEscola() {
        return escola;
    }

    public void setEscola(String escola) {
        this.escola = escola;
    }
    int idUsuario;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO UsuarioBD3 (id_usuario,nome_usuario, senha,NOME_ESCOLA ) VALUES (SEQ_USUARIOBD3_ID_USUARIO.nextval,?,?,?)";
        

        try {
            String generatedColumns[] = {"id_usuario"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            preparedStatement.setString(1, this.nome);
            preparedStatement.setString(2, this.senha);
            preparedStatement.setString(3, this.escola);
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setIdUsuario(chaveGerada);

        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public static ArrayList<Usuario> getAll() {
        String selectSQL = "SELECT * FROM UsuarioBD3";
        ArrayList<Usuario> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                Usuario f = new Usuario();
                f.setNome(rs.getString("nome_usuario"));
                f.setSenha(rs.getString("senha"));
                f.setIdUsuario(rs.getInt("id_usuario"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static int getId(String login) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        int b = 0;
        String selectSQL = "SELECT id_usuario FROM UsuarioBD3 WHERE nome_usuario = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, login);// 
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                b = rs.getInt("id_usuario");
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return b;
        }finally{
            c.desconecta(); 
        }         
        return b;
    }
    
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM UsuarioBD3 WHERE id_usuario = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.idUsuario);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public static boolean verificaExisteBanco(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM UsuarioBD3 WHERE nome_usuario = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);// 
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public static boolean verificaExisteBanco2(String nome, String senha) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM UsuarioBD3 WHERE nome_usuario = ? AND senha = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);// 
            ps.setString(2, senha);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
}
