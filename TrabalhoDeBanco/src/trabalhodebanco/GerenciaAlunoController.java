/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class GerenciaAlunoController implements Initializable {

    @FXML
    private Label mensagemResposta;
    @FXML
    private TableView<Aluno> tabela;
    @FXML
    private Button adicionaAlunoButton;
    @FXML
    private TextField matricula;
    @FXML
    private TextField nome;
    @FXML
    private Button excluirButton;
    @FXML
    private TableColumn<Aluno, Integer> matriculaCol;
    @FXML
    private TableColumn<Aluno, String> nomeCol;
    
    private ObservableList<Aluno> alunoList;
    
    private Aluno tipo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          atualiza();
          excluirButton.setDisable(true);
    }    

    @FXML
    private void voltaPaginaInicial(ActionEvent event) {
        Parent root;
        try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }

    @FXML
    private void limpaInputs(ActionEvent event) {
        tabela.getSelectionModel().clearSelection();
        nome.setText("");
        matricula.setText("");
        
        excluirButton.setDisable(true);
        mensagemResposta.setText("");
    }

    @FXML
    private void selecionado(MouseEvent event) {
        if(tabela.getSelectionModel().getSelectedItem() != null){
            excluirButton.setDisable(false);
            
            Aluno tipoSelecionado = tabela.getSelectionModel().getSelectedItem();
            
        }
    }

    @FXML
    private void adicionaAluno(ActionEvent event) {
        if(!nome.getText().isEmpty() && !matricula.getText().isEmpty() && matricula.getText().matches("^[0-9]*[.]{0,1}[0-9]*$")){
            if(!Aluno.verificaExisteBanco(Integer.valueOf(matricula.getText()))){
                   tipo = new Aluno();
                   tipo.setNome(nome.getText());
                   tipo.setMatricula(Integer.valueOf(matricula.getText()));
                   boolean resposta = tipo.insert();

                   
                   
                   if(resposta){
                        mensagemResposta.setTextFill(Color.GREEN);
                        mensagemResposta.setText("Dado Foi Cadastrado Corretamente");
                    }else{
                        mensagemResposta.setTextFill(Color.RED);
                        mensagemResposta.setText("Erro Ao Cadastrar");
                    }
                   
                   nome.setText("");
                   matricula.setText("");
                   
                   
                   alunoList.add(tipo);
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Esse MATRICULA Já Está Sendo Usado");
            }
        }else{
           mensagemResposta.setTextFill(Color.RED);
           mensagemResposta.setText("Não Deixe Nada Em Braco"); 
            
        }
    }


    @FXML
    private void excluir(ActionEvent event) {
        Aluno tipoSelecionado = tabela.getSelectionModel().getSelectedItem();
        boolean resposta = tipoSelecionado.delete();
        alunoList.remove(tipoSelecionado);
        limpaInputs(event);
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Aluno Excluido Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }
    }
    
    public void atualiza(){
        
        alunoList = tabela.getItems();
        alunoList.clear();
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        matriculaCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));
        Aluno.getAll().forEach((a) -> {
            alunoList.add(a);
        });
        
    }
    
}
