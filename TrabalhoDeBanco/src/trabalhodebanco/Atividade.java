/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author MaisUmaVez
 */
public class Atividade {
    private String nome,data;
    private int id;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO ATIVIDADEBD3 (ID_ATIVIDADE,NOME_ATIVIDADE,DATA,ID_USUARIO) VALUES (SEQ_ATIVIDADEBD3_ID_ATIVIDADE.nextval,?,?,?)";
        

        try {
            
            String generatedColumns[] = {"ID_ATIVIDADE"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL,generatedColumns);
            preparedStatement.setString(1, this.nome);
            preparedStatement.setString(2, this.data);
            
            preparedStatement.setInt(3, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setId(chaveGerada);
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<Atividade> getAll() {
        String selectSQL = "SELECT * FROM ATIVIDADEBD3 WHERE ID_USUARIO = ?";
        ArrayList<Atividade> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Atividade f = new Atividade();
                f.setId(rs.getInt("ID_ATIVIDADE"));
                f.setNome(rs.getString("NOME_ATIVIDADE"));
                f.setData(rs.getString("DATA"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    public static ArrayList<Aluno> getAllMembros(int grupo) {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ALUNOBD3.ID_ALUNO  IN (SELECT ID_ALUNO FROM ALUNO_ATIVIDADEBD3 WHERE  ID_ATIVIDADE = ?)";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, grupo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    public static ArrayList<Aluno> getAllNaoMembros(int grupo) {
        String selectSQL = "SELECT * FROM ALUNOBD3 WHERE ALUNOBD3.ID_ALUNO NOT IN (SELECT ID_ALUNO FROM ALUNO_ATIVIDADEBD3 WHERE  ID_ATIVIDADE = ?) ";
        ArrayList<Aluno> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, grupo);
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Aluno f = new Aluno();
                f.setNome(rs.getString("NOME_ALUNO"));
                f.setMatricula(rs.getInt("MATRICULA"));
                f.setId(rs.getInt("ID_ALUNO"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta();
        }   

        return lista;
    }
    
    
    
    
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM ATIVIDADEBD3 WHERE ID_ATIVIDADE = ? AND ID_USUARIO = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
          
            deleteMembros();
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
//    public static boolean verificaExisteBanco(String nome) {
//	Conexao c = new Conexao();
//        Connection dbConnection = c.getConexao();
//        PreparedStatement preparedStatement = null;
//
//        String selectSQL = "SELECT * FROM TURMASBD3 WHERE NOME_TURMA = ? AND ID_USUARIO  = ?";
//        PreparedStatement ps;
//        try { 
//            ps = dbConnection.prepareStatement(selectSQL);
//            ps.setString(1, nome);// 
//            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
//            ResultSet rs = ps.executeQuery();
//            if(rs.next()){
//                return true;
//                
//            }
//        }  catch (SQLException e) {
//            e.printStackTrace();
//            return false;
//        }finally{
//            c.desconecta(); 
//        }         
//        return false;
//    }
    
    public boolean insertMembro(int membro) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO ALUNO_ATIVIDADEBD3 (ID_ALUNO,ID_ATIVIDADE ) VALUES (?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, membro);
            
            preparedStatement.setInt(2, this.id);
          
            
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public boolean deleteMembro(int membro) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM ALUNO_ATIVIDADEBD3 WHERE ID_ALUNO = ? AND ID_ATIVIDADE = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, membro);
            ps.setInt(2, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
    public boolean deleteMembros() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM ALUNO_ATIVIDADEBD3 WHERE ID_ATIVIDADE = ? ";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
        
    }
    
}
