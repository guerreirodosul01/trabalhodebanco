/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author MaisUmaVez
 */
public class Professor {
    private String nome,area;
    private int id;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO PROFESSORBD3 (ID_PROFESSOR,NOME_PROFESSOR,AREA ,ID_USUARIO) VALUES (SEQ_PROFESSOR_ID.nextval,?,?,?)";
        

        try {
            
            String generatedColumns[] = {"ID_PROFESSOR"};
            preparedStatement = dbConnection.prepareStatement(insertTableSQL,generatedColumns);
            preparedStatement.setString(1, this.nome);
            preparedStatement.setString(2, this.area);
            preparedStatement.setInt(3, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
            }
            setId(chaveGerada);
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
    
    public static ArrayList<Professor> getAll() {
        String selectSQL = "SELECT * FROM PROFESSORBD3 WHERE ID_USUARIO = ?";
        ArrayList<Professor> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        PreparedStatement st;
        try {
            
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                Professor f = new Professor();
                f.setNome(rs.getString("NOME_PROFESSOR"));
                f.setArea(rs.getString("AREA"));
                f.setId(rs.getInt("ID_PROFESSOR"));
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }   

        return lista;
    }
    
    
    
//    public boolean update(String nome) {
//      	Conexao c = new Conexao();
//        Connection dbConnection = c.getConexao();
//        PreparedStatement ps = null;
//
//        String insertTableSQL = "UPDATE MARCAJDBC3 SET NOME =? WHERE NOME = ?";
//        
//
//        try {
//            ps = dbConnection.prepareStatement(insertTableSQL);
//            ps.setString(1, nome);
//            ps.setString(2, this.nome);
//          
//            
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            
//            e.printStackTrace();
//            return false;
//        }finally{
//            c.desconecta(); 
//        }        
//        return true;
//    }
    
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM PROFESSORBD3 WHERE ID_PROFESSOR = ? AND ID_USUARIO = ?";        
        
        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            deleteTurma();
            //deletePendrives();
            ps.executeUpdate();
        } catch (SQLException e) {
           
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }      
        
        return true;
        
    }
    
    private void deleteTurma() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM TURMASBD3 WHERE ID_PROFESSOR = ? AND ID_USUARIO = ?";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
          
            deleteMembros();
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
            
        }finally{
            c.desconecta(); 
        }        
        
        
    }
    
    private void deleteMembros() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM TURMA_ALUNOBD3 WHERE ID_TURMA IN  (SELECT ID_TURMA FROM TURMASBD3 WHERE ID_PROFESSOR = ?) ";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);
          
            
            ps.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        
    }
//    public boolean deletePendrives() {
//        Conexao c = new Conexao();
//        Connection dbConnection = c.getConexao();
//        PreparedStatement ps = null;
//
//        String insertTableSQL = "DELETE FROM PRODUTOJDBC3 WHERE ID_MARCA  = ? ";        
//
//        try {
//            ps = dbConnection.prepareStatement(insertTableSQL);
//            ps.setInt(1, this.getId());
//          
//            
//            ps.executeUpdate();
//        } catch (SQLException e) {
//           
//            e.printStackTrace();
//            return false;
//        }finally{
//            c.desconecta(); 
//        }        
//        return true;
//        
//    }
    
    public static boolean verificaExisteBanco(int id) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT * FROM PROFESSORBD3 WHERE ID_PROFESSOR = ? AND ID_USUARIO = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id); 
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery(); 
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public static boolean verificaExisteBanco2() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        

        String selectSQL = "SELECT * FROM PROFESSORBD3 WHERE ID_USUARIO = ?";
        PreparedStatement st;
        try { 
            st = dbConnection.prepareStatement(selectSQL);
            st.setInt(1, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return true;
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally{
            c.desconecta(); 
        }         
        return false;
    }
    
    public static int getIdPeloNome(String nome) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT ID_PROFESSOR FROM PROFESSORBD3 WHERE NOME_PROFESSOR = ? AND ID_USUARIO = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome); 
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery(); 
            if(rs.next()){
                return rs.getInt("ID_PROFESSOR");
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }finally{
            c.desconecta(); 
        }         
        return 0;
    }
    
    public static String getNomePeloId(int id) {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String selectSQL = "SELECT NOME_PROFESSOR FROM PROFESSORBD3 WHERE ID_PROFESSOR = ? AND ID_USUARIO = ?";
        PreparedStatement ps;
        try { 
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, id); 
            ps.setInt(2, TrabalhoDeBanco.dadosGerais.getUserLogado());
            ResultSet rs = ps.executeQuery(); 
            if(rs.next()){
                return rs.getString("NOME_PROFESSOR");
                
            }
        }  catch (SQLException e) {
            e.printStackTrace();
            return "";
        }finally{
            c.desconecta(); 
        }         
        return "";
    }
}
