/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class GerenciaAtividadeController implements Initializable {

    @FXML
    private TableView<Atividade> atividadesTable;
    @FXML
    private TableColumn<Atividade, String> nomeAtividadeCol;
    @FXML
    private TableColumn<Atividade, String> dataAtividadeCol;
    @FXML
    private TableView<Aluno> naoAlunosTable;
    @FXML
    private TableColumn<Aluno, String> nomeNaoAlunosCol;
    @FXML
    private TableColumn<Aluno, Integer> matriculaNaoAlunosCol;
    @FXML
    private TableView<Aluno> alunosTable;
    @FXML
    private TableColumn<Aluno, String> nomeAlunosCol;
    @FXML
    private TableColumn<Aluno, Integer> matriculaAlunosCol;
    @FXML
    private TextField nome;
    @FXML
    private Button cadastrarButton;
    @FXML
    private Label mensagemResposta;
    @FXML
    private Button excluirButton;
    @FXML
    private Button adiconarAoGrupoButton;
    @FXML
    private Button removerButton;
    @FXML
    private DatePicker data;
    
    private ObservableList<Atividade> grupoList;
    private ObservableList<Aluno> membrosList;
    private ObservableList<Aluno> naoMembrosList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualiza();
        excluirButton.setDisable(true);
        adiconarAoGrupoButton.setDisable(true);
        removerButton.setDisable(true);
    }   
    
    public void atualiza(){
        
        grupoList = atividadesTable.getItems();
        grupoList.clear();
        nomeAtividadeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        dataAtividadeCol.setCellValueFactory(new PropertyValueFactory<>("data"));
        Atividade.getAll().forEach((a) -> {
            grupoList.add(a);
        });
       
    }
    
    public void atualizaMembros(){
        membrosList = alunosTable.getItems();
        membrosList.clear();
        nomeAlunosCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        matriculaAlunosCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));     
        Atividade.getAllMembros(atividadesTable.getSelectionModel().getSelectedItem().getId()).forEach((a) -> {
            membrosList.add(a);
        });
        System.out.println(Turma.getAllNaoMembros(atividadesTable.getSelectionModel().getSelectedItem().getId()));
        System.out.println(Turma.getAllMembros(atividadesTable.getSelectionModel().getSelectedItem().getId()));
        naoMembrosList = naoAlunosTable.getItems();
        naoMembrosList.clear();
        nomeNaoAlunosCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        matriculaNaoAlunosCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));    
        Atividade.getAllNaoMembros(atividadesTable.getSelectionModel().getSelectedItem().getId()).forEach((a) -> {
            naoMembrosList.add(a);
        });

        
    }

    @FXML
    private void turmaSelecionado(MouseEvent event) {
        if(atividadesTable.getSelectionModel().getSelectedItem() != null){
        atualizaMembros();
        excluirButton.setDisable(false);
        }
    }

    @FXML
    private void naoAlunoSelecionado(MouseEvent event) {
        if(naoAlunosTable.getSelectionModel().getSelectedItem() != null){
        adiconarAoGrupoButton.setDisable(false);
        }
    }

    @FXML
    private void alunoSelecionado(MouseEvent event) {
        if(alunosTable.getSelectionModel().getSelectedItem() != null){
        removerButton.setDisable(false);
        }
    }

    @FXML
    private void cadastrar(ActionEvent event) {
        System.out.println(data.getValue());
        if(!nome.getText().isEmpty() && data.getValue() != null){
            if(false){
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Atividade Já Em Uso");
            }else{
            Atividade atividade = new Atividade();
            atividade.setNome(nome.getText());
            atividade.setData(String.valueOf(data.getValue()));
            
            
            boolean resulta = atividade.insert();
            if(resulta){
                mensagemResposta.setTextFill(Color.GREEN);
                mensagemResposta.setText("Cadastrado Com Sucesso");
            }else{
                mensagemResposta.setTextFill(Color.RED);
                mensagemResposta.setText("Erro Ao Cadastrar");
            }
            }
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Preencha Coretamente");
        }
        atualiza();
    }

    @FXML
    private void excluirTurma(ActionEvent event) {
        alunosTable.getSelectionModel().clearSelection();
        naoAlunosTable.getSelectionModel().clearSelection();
        membrosList.clear();
        naoMembrosList.clear();
        excluirButton.setDisable(true);
        adiconarAoGrupoButton.setDisable(true);
        removerButton.setDisable(true);
        boolean resposta = atividadesTable.getSelectionModel().getSelectedItem().delete();
        grupoList.remove(atividadesTable.getSelectionModel().getSelectedItem());
        atividadesTable.getSelectionModel().clearSelection();
        
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Atividade E Membros Foram Excluidos Corretamente");
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Excluir");
        }
        
        atualiza();
    }

    @FXML
    private void adiconarAoGrupo(ActionEvent event) {
        boolean resposta = atividadesTable.getSelectionModel().getSelectedItem().insertMembro(naoAlunosTable.getSelectionModel().getSelectedItem().getId());
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Adicionado Corretamente");
            alunosTable.getSelectionModel().clearSelection();
            naoAlunosTable.getSelectionModel().clearSelection();
            adiconarAoGrupoButton.setDisable(true);
            removerButton.setDisable(true);
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Adicionar");
        }
        atualizaMembros();
    }

    @FXML
    private void removerAluno(ActionEvent event) {
        boolean resposta = atividadesTable.getSelectionModel().getSelectedItem().deleteMembro(alunosTable.getSelectionModel().getSelectedItem().getId());
        if(resposta){
            mensagemResposta.setTextFill(Color.GREEN);
            mensagemResposta.setText("Dado Foi Removido Corretamente");
            alunosTable.getSelectionModel().clearSelection();
            naoAlunosTable.getSelectionModel().clearSelection();
            adiconarAoGrupoButton.setDisable(true);
            removerButton.setDisable(true);
        }else{
            mensagemResposta.setTextFill(Color.RED);
            mensagemResposta.setText("Erro Ao Remover");
        }
        atualizaMembros();
    }

    @FXML
    private void voltarInicio(ActionEvent event) {
        Parent root;
        try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        } 
    }
    
}
