/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhodebanco;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MaisUmaVez
 */
public class PesquisaController implements Initializable {

    @FXML
    private TableView<Atividade> atividadesTable;
    @FXML
    private TableColumn<Atividade, String> nomeAtividadeCol;
    @FXML
    private TableColumn<Atividade, String> dataAtividadeCol;
    @FXML
    private TableView<Aluno> alunosTable;
    @FXML
    private TableColumn<Aluno, String> nomeAlunosCol;
    @FXML
    private TableColumn<Aluno, Integer> matriculaAlunosCol;
    @FXML
    private TextField a1;
    @FXML
    private Button pesquisarButton;
    @FXML
    private Label mensagemResposta;
    @FXML
    private TableView<Turma> turmaTable;
    @FXML
    private TableColumn<Turma, String> nomeTurmaCol;
    @FXML
    private TableColumn<Turma, String> salaTurmaCol;
    @FXML
    private TableColumn<Turma, String> nivelTurmaCol;
    @FXML
    private TableColumn<Turma, String> professorTurmaCol;
    @FXML
    private TableView<Professor> tabela;
    @FXML
    private TableColumn<Professor, String> nomeCol;
    @FXML
    private TableColumn<Professor, String> areaCol;
    @FXML
    private TextField a2;
    @FXML
    private TextField a3;
    @FXML
    private TextField a4;
    @FXML
    private TextField a5;
    @FXML
    private TextField a6;
    @FXML
    private TextField a7;
    @FXML
    private TextField a8;
    @FXML
    private TextField a9;
    @FXML
    private Button turmaComMaisButton;
    @FXML
    private Button alunosSemButton;
    @FXML
    private Button turmaSemButton;
    @FXML
    private Button professorSemButton;
    
    private int questao;
    private ObservableList<Aluno> alunoList;
    private ObservableList<Professor> professorList;
    private ObservableList<Atividade> grupoList;
    private ObservableList<Turma> turmaList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomeAlunosCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        matriculaAlunosCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));
        nomeAtividadeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        dataAtividadeCol.setCellValueFactory(new PropertyValueFactory<>("data"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
        areaCol.setCellValueFactory(new PropertyValueFactory<>("area"));
        nomeTurmaCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        salaTurmaCol.setCellValueFactory(new PropertyValueFactory<>("sala"));
        nivelTurmaCol.setCellValueFactory(new PropertyValueFactory<>("nivel"));
        professorTurmaCol.setCellValueFactory(new PropertyValueFactory<>("nomeProfessor"));
        alunoList = alunosTable.getItems();
        professorList = tabela.getItems();
        turmaList = turmaTable.getItems();
        grupoList = atividadesTable.getItems();
        alunoList.clear();
        professorList.clear();
        turmaList.clear();
        grupoList.clear();
    }    

    @FXML
    private void b1(MouseEvent event) {
        limpar();
        a2.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 1;
    }

    @FXML
    private void Pesquisar(ActionEvent event) {
        a1.setDisable(false);
        a2.setDisable(false);
        a3.setDisable(false);
        a4.setDisable(false);
        a5.setDisable(false);
        a6.setDisable(false);
        a7.setDisable(false);
        a8.setDisable(false);
        a9.setDisable(false);
        turmaComMaisButton.setDisable(false);
        alunosSemButton.setDisable(false);
        turmaSemButton.setDisable(false);
        professorSemButton.setDisable(false);
        alunoList.clear();
        professorList.clear();
        turmaList.clear();
        grupoList.clear();
        switch(questao){
            case 1:
                
                System.out.println("teste " + a1.getText());
                Pesquisa.pesquisaAlunoPorNome(a1.getText()).forEach((a) -> {
                    alunoList.add(a);
                });
            break;
            case 2:
                Pesquisa.pesquisaAtividadePorNome(a2.getText()).forEach((a) -> {
                    grupoList.add(a);
                });
            break;
            case 3:
                Pesquisa.pesquisaProfessorPorNome(a3.getText()).forEach((a) -> {
                    professorList.add(a);
                });
            break;
            case 4:
                Pesquisa.pesquisaTurmaPorNome(a4.getText()).forEach((a) -> {
                    turmaList.add(a);
                });
            break;
            case 5:
                Pesquisa.pesquisaTurmaPorProfessor(a5.getText()).forEach((a) -> {
                    turmaList.add(a);
                });
            break;
            case 6:
                Pesquisa.pesquisaAtividadePorAluno(a6.getText()).forEach((a) -> {
                    grupoList.add(a);
                });
            break;
            case 7:
                Pesquisa.pesquisaTurmaPorArea(a7.getText()).forEach((a) -> {
                    turmaList.add(a);
                });
            break;
            case 8:
                Pesquisa.pesquisaAtividadePorData(a8.getText()).forEach((a) -> {
                    grupoList.add(a);
                });
            break;
            case 9:
                Pesquisa.pesquisaAlunoPorMatricula(Integer.valueOf(a9.getText())).forEach((a) -> {
                    alunoList.add(a);
                });
            break;
        }
        a1.setText("");
        a2.setText("");
        a3.setText("");
        a4.setText("");
        a5.setText("");
        a6.setText("");
        a7.setText("");
        a8.setText("");
        a9.setText("");
    }


    @FXML
    private void voltarInicio(ActionEvent event) {
        Parent root;
        try {

            Stage stage = TrabalhoDeBanco.stage;
            
            root = FXMLLoader.load(getClass().getResource("TelaAcao.fxml"));
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            
        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        }
    }

    @FXML
    private void b2(MouseEvent event) {
        a1.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 2;
    }

    @FXML
    private void b3(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 3;
    }

    @FXML
    private void b4(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 4;
        
    }

    @FXML
    private void b5(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 5;  
    }

    @FXML
    private void b6(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 6;
    }

    @FXML
    private void b7(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a8.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 7;
    }

    @FXML
    private void b8(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a9.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
       
        professorSemButton.setDisable(true);
        questao = 8;
    }

    @FXML
    private void b9(MouseEvent event) {
        a1.setDisable(true);
        a2.setDisable(true);
        a3.setDisable(true);
        a4.setDisable(true);
        a5.setDisable(true);
        a6.setDisable(true);
        a7.setDisable(true);
        a8.setDisable(true);
        turmaComMaisButton.setDisable(true);
        alunosSemButton.setDisable(true);
        turmaSemButton.setDisable(true);
        professorSemButton.setDisable(true);
        questao = 9;
        
    }

    @FXML
    private void turmaComMais(ActionEvent event) {
         limpar();
        Pesquisa.pesquisaTurmas().forEach((a) -> {
                    turmaList.add(a);
                });
       
    }

    @FXML
    private void alunosSem(ActionEvent event) {
        limpar();
        Pesquisa.pesquisaAlunoSem().forEach((a) -> {
                    alunoList.add(a);
                });
        
    }

    @FXML
    private void turmaSem(ActionEvent event) {
        limpar();
        Pesquisa.pesquisaTurmasSem().forEach((a) -> {
                    turmaList.add(a);
                });
        
    }

    

    @FXML
    private void professorSem(ActionEvent event) {
         limpar();
        Pesquisa.pesquisaProfessorSem().forEach((a) -> {
                    professorList.add(a);
                });
        
    }

    @FXML
    private void limpar() {
        a1.setDisable(false);
        a2.setDisable(false);
        a3.setDisable(false);
        a4.setDisable(false);
        a5.setDisable(false);
        a6.setDisable(false);
        a7.setDisable(false);
        a8.setDisable(false);
        a9.setDisable(false);
        a1.setText("");
        a2.setText("");
        a3.setText("");
        a4.setText("");
        a5.setText("");
        a6.setText("");
        a7.setText("");
        a8.setText("");
        a9.setText("");
        turmaComMaisButton.setDisable(false);
        alunosSemButton.setDisable(false);
        turmaSemButton.setDisable(false);
        professorSemButton.setDisable(false);
        alunoList.clear();
        professorList.clear();
        turmaList.clear();
        grupoList.clear();
    }

    
}
